var DMPTree = function () {

    return {
        //main function to initiate the module
        init: function () {

            var DataSourceTree = function (options) {
                this._data  = options.data;
                this._delay = options.delay;
            };

            DataSourceTree.prototype = {

                data: function (options, callback) {
                    var nodes;
                    if($.isEmptyObject(options)) {
                        nodes = this._data;
                    }
                    else {
                        if(options.children == null || options.children == undefined) {
                            nodes = {};
                        }
                        else {
                            nodes = options.children;
                        }
                    }

                    setTimeout(function () {
                        callback({ data: nodes });
                    }, this._delay)
                }
            };
            var treeDataSourceDynamicStr = $("#dataTreeSrc").val();
            var treeDataSourceDynamic = JSON.parse(treeDataSourceDynamicStr);

            var deparmentTreeSrc = new DataSourceTree({
                data: treeDataSourceDynamic,
                delay: 400
            });

            $('#DepartmentTree').tree({
                dataSource: deparmentTreeSrc,
                loadingHTML: '<img src="../static/images/input-spinner.gif"/>'
            });

        }

    };

}();