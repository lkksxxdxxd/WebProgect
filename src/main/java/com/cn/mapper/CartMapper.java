package com.cn.mapper;

import com.cn.dao.Cart;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CartMapper {
    /**
     * delete by primary key
     * @param bookid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Integer bookid);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Cart record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Cart record);

    /**
     * select by primary key
     * @param bookid primary key
     * @return object by primary key
     */
    Cart selectByPrimaryKey(Integer bookid);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Cart record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Cart record);
    @Insert("insert into cart(bookid, bookname, author, unitprice,quanties,username) VALUES (#{bookid},#{name},#{author},#{unitprice},#{num},#{username})")
    void insertBook(int bookid, String name, String author, int unitprice,int num,String username);
    @Select("select * from cart where username = #{username} and bookid = #{bookid}")
    Cart selectByUnAndBid(String username, int bookid);

    @Update("update cart set quanties = quanties+1 where username = #{username} and bookid = #{bookid}")
    void updateByUnAndBid(String username, int bookid);

    @Select("select * from cart where username = #{username}")
    List<Cart> selectCartInfo(String username);

    @Delete("delete from cart where username = #{username} and bookname = #{bookname}")
    void deletByUnAndBn(String username, String bookname);

    @Delete("delete from cart where username = #{username}")
    void deleteByUserName(String username);


}
