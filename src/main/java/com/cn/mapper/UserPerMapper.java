package com.cn.mapper;

import com.cn.dao.UserPer;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserPerMapper {
    int deleteByPrimaryKey(String user);

    int insert(UserPer record);

    int insertSelective(UserPer record);

    UserPer selectByPrimaryKey(String user);

    int updateByPrimaryKeySelective(UserPer record);

    int updateByPrimaryKey(UserPer record);
}