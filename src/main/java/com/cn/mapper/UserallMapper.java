package com.cn.mapper;

import com.cn.dao.Touxiang;
import com.cn.dao.Userall;
import com.cn.dao.UserallExample;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserallMapper {


    /**
     * delete by primary key
     * @param user primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String user);

    /**
     * insert record to table
     * @return insert count
     */
    @Insert("INSERT into userall(user, password,salt) VALUES (#{username} ,#{password} ,#{salt})")
    int insert(String username,String password,String salt);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Userall record);


    /**
     * select by primary key
     * @param user primary key
     * @return object by primary key
     */
    @Select("SELECT * from userall where user = #{user}")
    Userall selectByPrimaryKey(String user);


    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Userall record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Userall record);

    @Select("SELECT user from userall")
    String[] getUserName();

    @Select("select * from student")
    void getId();

    @Select("select * from userall where user = #{username} and password = #{password}")
    Userall selectByNameAndPassword(String username,String password);

    @Select("select perms FROM userPer WHERE user = #{user} ")
    String getUserPer(String user);

    @Insert("insert into userPer(user, perms) VALUES (#{username} ,#{perms})")
    void insertUserPer(String username, String perms);

    @Insert("insert into touxiang(id, photo) VALUES (#{id},#{name})")
    Touxiang insertTouXiang(Integer id, String name);

    @Select("select * from touxiang where id = #{id}")
    String getTouXiang(Integer id);


}