package com.cn.mapper;

import com.cn.dao.Storage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StorageMapper {
    /**
     * delete by primary key
     * @param bookname primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String bookname);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Storage record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Storage record);

    /**
     * select by primary key
     * @param bookname primary key
     * @return object by primary key
     */
    Storage selectByPrimaryKey(String bookname);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Storage record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Storage record);

    @Select("select * from Storage")
    List<Storage> selectAllStorage();
    @Select("select * from Storage where (bookname like CONCAT('%', #{bookinfo}, '%') or author like CONCAT('%', #{bookinfo}, '%')) and categories = #{category}")
    List<Storage> selectStBy(String bookinfo, String category);
    @Select("select * from Storage where bookname like CONCAT('%', #{bookinfo}, '%') or author like CONCAT('%', #{bookinfo}, '%')")
    List<Storage> selectByName(String bookinfo);
}
