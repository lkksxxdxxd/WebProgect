package com.cn.mapper;

import com.cn.dao.Touxiang;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TouxiangMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Touxiang record);

    int insertSelective(Touxiang record);

    Touxiang selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Touxiang record);

    int updateByPrimaryKey(Touxiang record);
}