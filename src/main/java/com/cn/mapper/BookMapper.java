package com.cn.mapper;

import com.cn.dao.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BookMapper {
    /**
     * delete by primary key
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Book record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Book record);

    /**
     * select by primary key
     * @param id primary key
     * @return object by primary key
     */
    Book selectByPrimaryKey(Integer id);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Book record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Book record);

    @Select("select * from Book")
    List<Book> selectAllBook();

    @Select("select * from Book where CategoryId = #{category} ")
    List<Book> selectBookByCategory(String category);

    @Select("select name from Book where Id = #{id} ")
    String selectBookById(int id);

    @Select("select unitprice from Book where Id = #{id} ")
    String selectBookPriceById(int id);

    @Select("select author from Book where Id = #{id} ")
    String selectBookAuthorById(int id);

    @Select("select contentdescription from Book where Id = #{id} ")
    String selectBookContentById(int id);
    @Select("select * from Book where Id = #{bookid} ")
    Book selectBookByids(int bookid);

    @Select("select Id from Book where Name = #{bookname} and Author = #{author}")
    Integer selectIdByBnAndAu(String bookname, String author);
}
