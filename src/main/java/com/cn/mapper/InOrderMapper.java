package com.cn.mapper;

import com.cn.dao.InOrder;
import com.cn.dao.OutOrder;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

@Mapper
public interface InOrderMapper {
    /**
     * delete by primary key
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(InOrder record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(InOrder record);

    /**
     * select by primary key
     * @param id primary key
     * @return object by primary key
     */
    InOrder selectByPrimaryKey(Integer id);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(InOrder record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(InOrder record);


    @Select("select * from InOrder")
    List<InOrder> selectAllInOrder();

    @Select("select * from outOrder")
    List<OutOrder> selectAllOutOrder();

    @Insert("insert into outOrder(bookname, username, quantity, unitprice, orderdate, orderstatename) " +
            "VALUES (#{bookname},#{username},#{quantity},#{unitprice},#{date},#{orderstate})")
    void insertOrder(String bookname, String username,int quantity, int unitprice, Date date, String orderstate);
}
