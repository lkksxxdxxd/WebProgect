package com.cn.mapper;

import com.cn.dao.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {
    /**
     * delete by primary key
     * @param userid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("userid") Integer userid, @Param("email") String email, @Param("nickname") String nickname);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(User record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(User record);

    /**
     * select by primary key
     * @param userid primary key
     * @return object by primary key
     */
    User selectByPrimaryKey(@Param("userid") Integer userid, @Param("email") String email, @Param("nickname") String nickname);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(User record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(User record);

    @Select("select * from User where nickname = #{username} and password = #{password}")
    User selectByUnAndPs(String username, String password);
    @Select("select * from User where nickname = #{username} ")
    User selectByName(String username);

    @Insert("insert into User(nickname,password,salt) values (#{username},#{NewPassword},#{Salt})")
    int insertUser(String username,String NewPassword,String Salt);

    @Select("select * from User")
    List<User> selectUserInfo();


}
