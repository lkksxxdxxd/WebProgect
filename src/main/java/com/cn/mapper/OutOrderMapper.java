package com.cn.mapper;

import com.cn.dao.OutOrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OutOrderMapper {
    /**
     * delete by primary key
     * @param orderid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Integer orderid);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(OutOrder record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(OutOrder record);

    /**
     * select by primary key
     * @param orderid primary key
     * @return object by primary key
     */
    OutOrder selectByPrimaryKey(Integer orderid);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(OutOrder record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(OutOrder record);
}