package com.cn.dao;

public class Book {
    /**
    * 图书编号
    */
    private Integer id;

    /**
    * 图书标题
    */
    private String name;

    private String author;

    private String categoryid;

    private Integer unitprice;

    private Integer quantity;

    private String contentdescription;

    private byte[] bookimage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public Integer getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(Integer unitprice) {
        this.unitprice = unitprice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getContentdescription() {
        return contentdescription;
    }

    public void setContentdescription(String contentdescription) {
        this.contentdescription = contentdescription;
    }

    public byte[] getBookimage() {
        return bookimage;
    }

    public void setBookimage(byte[] bookimage) {
        this.bookimage = bookimage;
    }
}
