package com.cn.dao;
public class Userall{

    private String user;

    private String password;
    private String salt;
    public Userall(){}

    public Userall(Userall user1) {
        this.user = user1.user;
        this.password = user1.password;
    }

    public Userall(String username, String password) {
        this.user = username;
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getSalt(){
        return salt;
    }
    public void setSalt(String salt){
        this.salt = salt;
    }
}