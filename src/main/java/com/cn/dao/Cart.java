package com.cn.dao;

public class Cart {
    private Integer bookid;

    private String bookname;

    private String author;

    private Integer unitprice;

    private String username;

    private Integer quanties;

    public Integer getBookid() {
        return bookid;
    }

    public void setBookid(Integer bookid) {
        this.bookid = bookid;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(Integer unitprice) {
        this.unitprice = unitprice;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getQuanties() {
        return quanties;
    }

    public void setQuanties(Integer quanties) {
        this.quanties = quanties;
    }
}