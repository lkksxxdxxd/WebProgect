package com.cn.dao;

public class Student {
    private String name;

    private String sex;

    private Integer age;

    private String school;

    private String major;

    private String no;

    public Student(String name, int age, String sex, String school, String major) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.school = school;
        this.major = major;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
    public Student(String name,int age,String sex,String school,String major,String no){
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.no = no;
        this.major = major;
        this.school = school;
    }
    @Override
    public String toString(){
        return this.name + " "+ this.age + " "+this.school;
    }
}