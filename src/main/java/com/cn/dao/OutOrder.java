package com.cn.dao;

import java.util.Date;

public class OutOrder {
    private Integer orderid;

    private String bookname;

    private String username;

    private Integer quantity;

    private Double unitprice;

    private Date orderdate;

    private String orderstatename;

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(Double unitprice) {
        this.unitprice = unitprice;
    }

    public Date getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(Date orderdate) {
        this.orderdate = orderdate;
    }

    public String getOrderstatename() {
        return orderstatename;
    }

    public void setOrderstatename(String orderstatename) {
        this.orderstatename = orderstatename;
    }
}