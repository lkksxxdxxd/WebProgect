package com.cn.dao;

public class Storage {
    private String bookname;

    private Integer quanties;

    private String author;

    private String categories;

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public Integer getQuanties() {
        return quanties;
    }

    public void setQuanties(Integer quanties) {
        this.quanties = quanties;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }
}