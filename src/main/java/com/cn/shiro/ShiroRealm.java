package com.cn.shiro;

import com.cn.dao.User;
import com.cn.dao.UserPer;
import com.cn.dao.Userall;
import com.cn.service.Impl.userCheckServiceImpl;
import com.cn.service.Impl.userLoginServerImpl;
import javafx.beans.property.ReadOnlyListProperty;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

public class ShiroRealm extends AuthorizingRealm {
    @Autowired
    userCheckServiceImpl userCheckService;
    @Autowired
    User user;
    @Autowired
    userLoginServerImpl userLoginServer;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("执行授权：Authorization");


        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();


        Subject subject = SecurityUtils.getSubject();
        try{
            Userall userall = (Userall) subject.getPrincipal();
            String role1 = userCheckService.getUserPer(userall.getUser());
            info.addStringPermission(role1);
        }catch (Exception e){
            User user1 = (User) subject.getPrincipal();
            String role2 = userCheckService.getUserPer(user1.getNickname());
            info.addStringPermission(role2);
        }
        return info;
    }

    /**
     * 执行认证逻辑
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("执行认证：Authentiction");
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        //假设数据库的用户名和密码
        Userall user2 = userCheckService.selectByKeyName(token.getUsername());

        if (user2 == null) {
            User user1 = userLoginServer.selectUserByName(token.getUsername());
            String DBPassword = user1.getPassword();
            String Salt = user1.getSalt();
            System.out.println(Salt);
            return new SimpleAuthenticationInfo(user1, DBPassword, ByteSource.Util.bytes(Salt), "");
        } else {
            String DBPassword = user2.getPassword();
            String Salt = user2.getSalt();
            System.out.println(Salt);
            return new SimpleAuthenticationInfo(user2, DBPassword, ByteSource.Util.bytes(Salt), "");
        }

    }
}
