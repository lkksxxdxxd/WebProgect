package com.cn.config;

import com.cn.dao.*;
import com.cn.service.Impl.*;

import com.github.pagehelper.PageHelper;
import org.apache.catalina.Store;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @Configuration 注解用于定义一个配置类，相当于 Spring 的配置文件
 * 配置类中包含一个或多个被 @Bean 注解的方法，该方法相当于 Spring 配置文件中的 <bean> 标签定义的组件。
 */
@Configuration
public class MyAppConfig {
    /**
     * 与 <bean id="personService" class="PersonServiceImpl"></bean> 等价
     * 该方法返回值以组件的形式添加到容器中
     * 方法名是组件 id（相当于 <bean> 标签的属性 id)
     */

    @Bean
    public userCheckServiceImpl userCheckService(){
        return new userCheckServiceImpl();
    }
    @Bean
    public Userall userall(){
        return new Userall();
    }
    @Bean
    public UserPer userPer(){
        return new UserPer();
    }

    @Bean
    public PageHelper pageHelper() {
        PageHelper pageHelper = new PageHelper();
        Properties p = new Properties();
        p.setProperty("dialect", "mysql");
        p.setProperty("offsetAsPageNum", "true");
        p.setProperty("rowBoundsWithCount", "true");
        pageHelper.setProperties(p);
        return pageHelper;
    }
    @Bean
    public Book book(){
        return new Book();
    }
    @Bean
    public BookServerImpl bookServer(){
        return new BookServerImpl();
    }

    @Bean
    public InOrderServerImpl inOrderServer(){
        return new InOrderServerImpl();
    }
    @Bean
    public InOrder inOrder(){
        return new InOrder();
    }
    @Bean
    public OutOrder outOrder(){
        return new OutOrder();
    }
    @Bean
    public User user(){
        return new User();
    }
    @Bean
    public userLoginServerImpl userLoginServer(){
        return new userLoginServerImpl();
    }
    @Bean
    public Cart cart(){
        return new Cart();
    }
    @Bean
    public CartServerImpl cartServer(){
        return new CartServerImpl();
    }
    @Bean
    public Storage storage(){
        return new Storage();
    }
    @Bean
    public storageServerImpl storageServer(){
        return new storageServerImpl();
    }
    @Bean
    public UserServerImpl userServer(){
        return new UserServerImpl();
    }
}
