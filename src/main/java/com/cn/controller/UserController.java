package com.cn.controller;

import antlr.StringUtils;
import com.cn.dao.*;
import com.cn.service.BookServer;
import com.cn.service.Impl.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.org.apache.xpath.internal.operations.Mod;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.io.FileUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.jws.WebParam;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class UserController {
    private BeanListHandler<Student> handler = new BeanListHandler<>(Student.class);
    @Autowired
    userCheckServiceImpl userCheckServiceImpl;
    // @ResponseBody

    @Autowired
    userLoginServerImpl userLoginServerImpl;
    @Autowired
    Book book;
    @Autowired
    BookServerImpl bookServerImpl;
    @Autowired
    InOrder inOrder;
    @Autowired
    InOrderServerImpl inOrderServerImpl;
    @Autowired
    OutOrder outOrder;
    @Autowired
    User user;
    @Autowired
    Cart cart;
    @Autowired
    CartServerImpl cartServerImpl;
    @Autowired
    Storage storage;
    @Autowired
    storageServerImpl storageServer;
    @Autowired
    UserServerImpl userServerImpl;


    @RequestMapping("/index1/cartBack")
    public String cartBack(HttpServletRequest httpServletRequest,Model model){
        String username = "17669473772";
        // Cookie以数组的形式保存在Request中
        Cookie[] cks = httpServletRequest.getCookies();
        if (cks != null) {
            for(Cookie ck : cks){
                if (ck.getName().equals("username")) {
                    username = ck.getValue();
                    break;
                }
            }
        }
        user = userLoginServerImpl.selectUserByName(username);
        List<Cart> cartInfoTmp=cartServerImpl.selectCartInfo(username);
        for(int i = 0;i < cartInfoTmp.size();i++){
            Cart cartTmp = cartInfoTmp.get(i);
            String bookname = cartTmp.getBookname();
            int quantity = cartTmp.getQuanties();
            int unitprice = cartTmp.getUnitprice();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");//注意月和小时的格式为两个大写字母
            java.util.Date date = new Date();//获得当前时间
            String orderdate = df.format(date);//将当前时间转换成特定格式的时间字符串，这样便可以插入到数据库中
            String orderstate = "订单生成在审核中";
            inOrderServerImpl.settlement(bookname,username,quantity,unitprice,orderdate,orderstate);

        }
        cartServerImpl.deletByUserName(username);
        List<Cart> cartInfo=cartServerImpl.selectCartInfo(username);
        model.addAttribute("cartInfo",cartInfo);
        return "cart";
    }

    @GetMapping(value = "/index/userManager")
    public String userManager(Model model){
        List<User> userInfo=userServerImpl.selectUserInfo();
        model.addAttribute("userInfo",userInfo);
        return "userManager";
    }

    @RequestMapping("/index/inbook")
    public String inbookPage(){
        return "inbook";
    }


    @RequestMapping("/index/uploadPicture")
    public String uploadPicture(ModelMap map, MultipartFile fileUpload,Model model) {
        String fileDir1 = "C:/games/WebProgect/src/main/resources/static/assets/img/gallery/";
        String fileDir2 = "C:/games/WebProgect/src/main/resources/templates/assets/img/gallery/";
        String filename = fileUpload.getOriginalFilename();
        //String suffixname = filename.substring(filename.lastIndexOf('.'));
        //filename = suffixname;

        //System.out.println(filename);
        File file = new File(fileDir1);
        if(!file.exists()) {
            file.mkdir();
        }
        File file2 = new File(fileDir1 + filename);
        try {
            FileUtils.copyInputStreamToFile(fileUpload.getInputStream(), file2);
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        File file1 = new File(fileDir2);
        if(!file1.exists()) {
            file1.mkdir();
        }
        File file22 = new File(fileDir2 + filename);
        try {
            FileUtils.copyInputStreamToFile(fileUpload.getInputStream(), file22);
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        map.addAttribute("imgDirectory", filename);
        model.addAttribute("bookloc",filename);
        return "inbook";
    }

    @RequestMapping("/index/kucunfind")
    public String kuncunFindPage(String bookinfo,@RequestParam(defaultValue = "0") int kind,Model model,@RequestParam(defaultValue = "1") Integer pageNum){
        String[] sTmp = {"小说", "儿童读物", "社会科学",
                "专业书", "名著", "人物传记", "历史书籍"};

        if(kind == 0){
            PageHelper.startPage(pageNum, 10);
            List<Storage>storageList = storageServer.selectStByName(bookinfo);
            PageInfo<Storage> pageInfo = new PageInfo<Storage>(storageList);
            model.addAttribute("pageInfo", pageInfo);
            model.addAttribute("storageList", storageList);
            return "kucun";
        }else {
            String category = sTmp[(kind - 1)];
            System.out.println(category);
            System.out.println(bookinfo);
            PageHelper.startPage(pageNum, 10);
            List<Storage>storageList = storageServer.selectStBy(bookinfo,category);
            PageInfo<Storage> pageInfo = new PageInfo<Storage>(storageList);
            model.addAttribute("pageInfo", pageInfo);
            model.addAttribute("storageList", storageList);
            return "kucun";
        }

    }
    @RequestMapping("/index/kucun")
    public String kucunPage(@RequestParam(defaultValue = "1") Integer pageNum, Model model){
        List<Storage> storageList;
        PageHelper.startPage(pageNum, 10);
        storageList = storageServer.selectAllStorage();
        PageInfo<Storage> pageInfo = new PageInfo<Storage>(storageList);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("storageList", storageList);
        return "kucun";
    }
    @RequestMapping("/userlogin")
    public String userLoginPage() {
        return "userlogin";
    }

    @RequestMapping("/indexUser")
    public String indexUserPage(String username, String password, HttpServletResponse httpServletResponse) {
        user = userLoginServerImpl.selectUserByUnAndPs(username, password);
        if (user != null) {
            Cookie cookie1 = new Cookie("username", username);
            Cookie cookie2 = new Cookie("password", password);
            cookie1.setMaxAge(60 * 30);
            httpServletResponse.addCookie(cookie1);
            httpServletResponse.addCookie(cookie2);
            return "index";
        } else {
            return "userlogin";
        }
    }


    @GetMapping(value = "/index1/bookdetails")
    public String bookDetails(Model model, int bookid) {
        String tmpString  = String.valueOf(bookid);
        String  bookname = bookServerImpl.selectBookByid(bookid);
        String bookprice = bookServerImpl.selectBookPriceByid(bookid);
        tmpString += ".jpg";
        String price="$";
        price+=String.valueOf(bookprice);
        price+=".00";

        String bookauthor=bookServerImpl.selectBookAuthorByid(bookid);
        String bookcontent=bookServerImpl.selectBookContentById(bookid);
        model.addAttribute("bookid",bookid);
        model.addAttribute("bookloc",tmpString);
        model.addAttribute("bookname",bookname);
        model.addAttribute("bookprice",price);
        model.addAttribute("bookauthor",bookauthor);
        model.addAttribute("bookcontent",bookcontent);
        System.out.println(bookid);
        return "bookdetails";
    }
    @GetMapping(value = "/index/cart")
    public String cartPage(Model model,@RequestParam(defaultValue = "1")int bookid,HttpServletRequest httpServletRequest){
        String  bookname = bookServerImpl.selectBookByid(bookid);
        String username = "17669473772";
        // Cookie以数组的形式保存在Request中
        Cookie[] cks = httpServletRequest.getCookies();
        if (cks != null) {
            for(Cookie ck : cks){
                if (ck.getName().equals("username")) {
                    username = ck.getValue();
                    break;
                }
            }
        }
        List<Cart> cartInfo=cartServerImpl.selectCartInfo(username);
        model.addAttribute("cartInfo",cartInfo);
        return "cart";
    }

    @RequestMapping("/index/bookdetailBack")
    public String bookdetailBack(int bookid,Model model,HttpServletRequest httpServletRequest){
        String username = "17669473772";
        // Cookie以数组的形式保存在Request中
        Cookie[] cks = httpServletRequest.getCookies();
        if (cks != null) {
            for(Cookie ck : cks){
                if (ck.getName().equals("username")) {
                    username = ck.getValue();
                    break;
                }
            }
        }
        System.out.println(username);

        book = bookServerImpl.selectBookByids(bookid);
        Cart cartTmp = cartServerImpl.selectByUnAndBid(username,bookid);
        if(cartTmp != null){
            cartServerImpl.updateByUnAndBid(username,bookid);
        }else {
            cartServerImpl.insertBook(book,username);
        }

        String tmpString  = String.valueOf(bookid);
        String  bookname = bookServerImpl.selectBookByid(bookid);
        String bookprice = bookServerImpl.selectBookPriceByid(bookid);
        tmpString += ".jpg";
        String price="$";
        price+=String.valueOf(bookprice);
        price+=".00";

        String bookauthor=bookServerImpl.selectBookAuthorByid(bookid);
        String bookcontent=bookServerImpl.selectBookContentById(bookid);
        model.addAttribute("bookid",bookid);
        model.addAttribute("bookloc",tmpString);
        model.addAttribute("bookname",bookname);
        model.addAttribute("bookprice",price);
        model.addAttribute("bookauthor",bookauthor);
        model.addAttribute("bookcontent",bookcontent);
        System.out.println(bookid);
        return "bookdetails";
    }
    @RequestMapping("/index/outorder")
    public String outorderPage(Model model, @RequestParam(defaultValue = "1") Integer pageNum) {
        List<OutOrder> outOrderList;
        PageHelper.startPage(pageNum, 5);
        outOrderList = inOrderServerImpl.selectAllOutorder();
        PageInfo<OutOrder> pageInfo = new PageInfo<OutOrder>(outOrderList);

        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("outorder", outOrderList);
        System.out.println("数量" + outOrderList.size());
        for (int i = 0; i < outOrderList.size(); i++) {
            System.out.println(outOrderList.get(i).getOrderid());
        }
        return "outorder";
    }

    @RequestMapping("/index/inorder")
    public String inorderPage(Model model, @RequestParam(defaultValue = "1") Integer pageNum) {
        List<InOrder> inOrderList;
        ArrayList<String> nameList = new ArrayList<String>();
        PageHelper.startPage(pageNum, 2);
        inOrderList = inOrderServerImpl.selectAllInorder();
        PageInfo<InOrder> pageInfo = new PageInfo<InOrder>(inOrderList);

        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("inorder", inOrderList);
        System.out.println("数量" + inOrderList.size());
        for (int i = 0; i < inOrderList.size(); i++) {
            System.out.println(inOrderList.get(i).getId());
        }
        return "inorder";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String indexPage(HttpServletResponse httpServletResponse,String username, String password, Map<String, Object> map, HttpSession session, HttpServletRequest request, Model model) {
        /**
         * 使用shiro编写认证的操作
         */
        //1.获取Subject
        Subject subject = SecurityUtils.getSubject();
        //2.封装用户数据
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
//3.执行登录方法
            subject.login(token);
            subject.getSession().setAttribute("username", username);
            model.addAttribute("username", username);
            session.setAttribute("username", username);
            Cookie cookie = new Cookie("username",username);
            httpServletResponse.addCookie(cookie);
            return "index";
        } catch (UnknownAccountException e) {
            //登录失败，用户名错误
            model.addAttribute("msg", "用户名不存在！");
            return "login";
        } catch (IncorrectCredentialsException e) {
            //登录失败，密码错误
            model.addAttribute("msg", "用户名或密码错误！");
            return "login";
        }

    }

    @RequestMapping(value = "/index1/Book", method = RequestMethod.GET)
    public String bookList(@RequestParam(defaultValue = "1") Integer pageNum, Model model) {
        List<Book> bookList;
        PageHelper.startPage(pageNum, 10);
        bookList = bookServerImpl.selectAllBook();
        PageInfo<Book> pageInfo = new PageInfo<Book>(bookList);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("book", bookList);
        System.out.println("数量" + bookList.size());
        for (int i = 0; i < bookList.size(); i++) {
            System.out.println(bookList.get(i).getName());
        }
        return "bookList";
    }

    @RequestMapping("/iindex")
    public String iindex() {
        return "index";
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public String bookListFind(@RequestParam(defaultValue = "1") Integer pageNum, Model model, int kind, String message) {
        String[] sTmp = {"小说", "儿童读物", "社会科学",
                "专业书", "名著", "人物传记", "历史书籍"};

        String category = sTmp[(kind - 1) > 0 ? kind - 1 : 1];

        List<Book> bookList;
        PageHelper.startPage(pageNum, 20);
        bookList = bookServerImpl.selectBookByCategory(category);
        PageInfo<Book> pageInfo = new PageInfo<Book>(bookList);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("book", bookList);
        model.addAttribute("kind", kind);
        System.out.println("数量" + bookList.size());
        for (int i = 0; i < bookList.size(); i++) {
            System.out.println(bookList.get(i).getName());
        }
        return "bookList";
    }

    @GetMapping(value = "/index/mail")
    public String mailPage() {
        return "mail";
    }

    @RequestMapping("/unauth")
    @ResponseBody
    public String unauth() {
        return "未授权没有访问权限";
    }

    @RequestMapping(value = "/reg", method = RequestMethod.GET)
    public String regPage() {
        return "reg";
    }


    @GetMapping(value = "/index/indexBack")
    public String indexBack() {
        return "redirect:/index.html";
    }


    @RequestMapping(value = "/index/back")
    public String BackToIndex() {
        return "redirect:/index.html";
    }


    @ApiOperation("上传头像")
    @RequestMapping(value = "/index/headImg", method = RequestMethod.POST)
    @ResponseBody
    public Object headImg(MultipartFile file) {
        // 将文件接收后，存放一个固定地方，然后将路径拿过来
        Subject subject = SecurityUtils.getSubject();
        Userall userall = (Userall) subject.getPrincipal();

        Integer id = Integer.parseInt(userall.getUser());
        String name = file.getOriginalFilename();

        Touxiang touxiang = userCheckServiceImpl.insertTouXiang(id, name);
        try {
            // 存放在D盘
            file.transferTo(new File("C://" + name));
//            Touxiang
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
        return "OK";
    }

    @RequestMapping(value = "/login")
    public String regPageToLogin(String username, String password, Map<String, Object> map, HttpSession session, Model model) {
        System.out.println(username);
        User user = new User(username, password);
        if (username == null || password == null) {
            return "reg";
        }
        if (password.length() < 4 || password.length() > 10) {
            return "reg";
        }
        User user1 = userLoginServerImpl.selectUserByName(username);
        if (user1 != null) {
            map.put("error", "用户名重复");
            return "reg";
        }
        try {
            //Integer id = Integer.parseInt(username);
            int flag = userLoginServerImpl.insert(username, password);
            System.out.println(flag);
            if (flag == 1) {
                //设置用户权限

                userCheckServiceImpl.insertUserPer(username, "Intern");
                return "login";
            } else {
                User usercheck = userLoginServerImpl.selectUserByUnAndPs(username, password);
                if (usercheck == null) {
                    return "reg";
                } else {
                    return "login";
                }
            }
        } catch (Exception e) {
            map.put("error", "用户名不符合数字格式");
            return "reg";
        }
    }
    @RequestMapping("/logout")
    public String Out(HttpServletRequest httpServletRequest){
        Cookie[] cks = httpServletRequest.getCookies();
        if (cks != null) {
            for(Cookie ck : cks){
                if (ck.getName().equals("username")) {
                    ck.setValue("NONE");
                    break;
                }
            }
        }
        return "redirect:/";
    }
    @RequestMapping("/logincheck")
    public String LoginCheck(HttpServletRequest httpServletRequest) {
        String username = "NONE";
        Cookie[] cks = httpServletRequest.getCookies();
        int flag = 0;

        if (cks != null) {
            for (Cookie ck : cks) {
                if (ck.getName().equals("username")) {
                    if (username.equals(ck.getValue())) {
                        flag = 1;

                    }
                }
            }
        }else if(cks == null){
            return "redirect:/";
        }
        if (flag == 0) {
            return "index";
        }
        return "index";
    }
}
