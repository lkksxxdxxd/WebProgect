package com.cn.utils;

/**
 * 数据库连接相关常数
 * 
 * @author snow1k
 * @date 2021/10/31
 */
public final class JdbcConstants {
    public static final String url =
        "jdbc:mysql://localhost:3306/myweb?characterEncoding=UTF-8&useSSL=false&autoReconnect=true&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
    public static final String user = "root";
    public static final String pwd = "123456";
}
