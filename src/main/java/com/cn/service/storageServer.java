package com.cn.service;

import com.cn.dao.Storage;

import java.util.List;

public interface storageServer {
    List<Storage> selectAllStorage();

    List<Storage> selectStBy(String bookinfo, String category);

    List<Storage> selectStByName(String bookinfo);
}
