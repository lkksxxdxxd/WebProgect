package com.cn.service;

import com.cn.dao.Book;
import com.cn.dao.Storage;

import java.util.List;

public interface BookServer {
    public List<Book> selectAllBook();

    List<Book> selectBookByCategory(String category);

    String selectBookByid(int id);
    String selectBookPriceByid(int id);
    String selectBookAuthorByid(int id);
    String selectBookContentById(int id);

    Book selectBookByids(int bookid);

    Integer selectBookByNameAndAuthor(String bookname, String author);
}
