package com.cn.service;
import com.cn.dao.Student;
import org.springframework.data.domain.Page;

import java.util.List;

public interface stuCheckService {
    public List<Student> selectByName(String name);

    int addStudent(Student student);

    void delStu(Student student);

    Student select(String name, String sex, int age, String school);

    List<Student> selectByMajor(String message);

    List<Student> selectBySchool(String message);

    List<Student> selectById(String message);

    List<Student> selectByAge(String message);

    List<Student> selectBySex(String message);

    List<Student> getAll();


    int delteByIds(String[] ids);

    List<Student> getAllStudent();

    Page<Student> getStudent(int pageNum, int pageSize);

    Student selectByIdOne(String id);

    void updataStudent(String name,String sex,int age,String school,String major,String no);

    boolean deleteById(String no);

}
