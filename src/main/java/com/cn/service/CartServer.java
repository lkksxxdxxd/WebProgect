package com.cn.service;

import com.cn.dao.Book;
import com.cn.dao.Cart;

import java.util.List;

public interface CartServer {

    void insertBook(Book book,String username);

    Cart selectByUnAndBid(String username, int bookid);

    void updateByUnAndBid(String username, int bookid);
    List<Cart> selectCartInfo(String username);

    void deletByUnAndBn(String username, String bookname);

    void deletByUserName(String username);
}
