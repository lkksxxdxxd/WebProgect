package com.cn.service.Impl;

import com.cn.dao.InOrder;
import com.cn.dao.OutOrder;
import com.cn.mapper.InOrderMapper;
import com.cn.service.InorderServer;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class InOrderServerImpl implements InorderServer {
    @Autowired
    private InOrderMapper inOrderMapper;


    @Override
    public List<InOrder> selectAllInorder() {
        return inOrderMapper.selectAllInOrder();
    }

    @Override
    public List<OutOrder> selectAllOutorder() {
        return inOrderMapper.selectAllOutOrder();
    }

    @Override
    public void settlement(String bookname, String username,int quantity, int unitprice, String orderdate, String orderstate) {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = ft.parse(orderdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        inOrderMapper.insertOrder(bookname,username,quantity,unitprice,date,orderstate);
    }
}
