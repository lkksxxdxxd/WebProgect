package com.cn.service.Impl;

import com.cn.dao.User;
import com.cn.mapper.UserMapper;
import com.cn.service.UserServer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserServerImpl implements UserServer {
    @Autowired
    UserMapper userMapper;

    @Override
    public List<User> selectUserInfo() {
        return userMapper.selectUserInfo();
    }
}
