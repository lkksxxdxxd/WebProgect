package com.cn.service.Impl;
import com.cn.dao.Touxiang;
import com.cn.dao.Userall;
import com.cn.mapper.UserallMapper;
import com.cn.service.userCheckService;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.tomcat.jni.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class userCheckServiceImpl implements userCheckService{

    @Autowired
    private UserallMapper userallMapper;


    @Override
    public boolean insert(String username, String password) {
        String Salt = new SecureRandomNumberGenerator().nextBytes().toHex();
        //加密
        SimpleHash simpleHash = new SimpleHash("md5",password,Salt,1);
        String NewPassword = simpleHash.toString();
        //System.out.println(password);
        userallMapper.insert(username,NewPassword,Salt);
        return true;
    }

    @Override
    public String[] getUsername() {
        return userallMapper.getUserName();
    }
    //检查是否存在此管理员
    @Override
    public boolean UserExitCheck(String[] Users,String tempUser){
        for(int i = 0;i < Users.length;i++){
            if(tempUser.equals(Users[i])){
                return true;
            }
        }
        return false;
    }

    @Override
    public Userall selectByNameAndPassword(String username, String password) {
        return userallMapper.selectByNameAndPassword(username,password);
    }

    @Override
    public Userall selectByKeyName(String user) {
        return userallMapper.selectByPrimaryKey(user);
    }

    @Override
    public void getId() {
        userallMapper.getId();
    }

    @Override
    public String getUserPer(String user) {
        return userallMapper.getUserPer(user);
    }

    @Override
    public void insertUserPer(String username, String perms) {
        userallMapper.insertUserPer(username,perms);
    }

    @Override
    public Touxiang insertTouXiang(Integer id, String name) {
        return userallMapper.insertTouXiang(id,name);
    }

    @Override
    public String getTouXiang(Integer id) {
        return userallMapper.getTouXiang(id);
    }
}
