package com.cn.service.Impl;

import com.cn.dao.Book;
import com.cn.dao.Storage;
import com.cn.mapper.BookMapper;
import com.cn.service.BookServer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BookServerImpl implements BookServer {
    @Autowired
    BookMapper bookMapper;
    @Override
    public List<Book> selectAllBook() {
        List<Book>bookList = bookMapper.selectAllBook();
        for(int i = 0;i < bookList.size();i++){
            String sTmp = bookList.get(i).getContentdescription();
            if(sTmp.length() > 20){
                bookList.get(i).setContentdescription(sTmp.substring(0,sTmp.length()/2));
            }
        }

        return bookList;
    }

    @Override
    public List<Book> selectBookByCategory(String category) {
        List<Book>bookList = bookMapper.selectBookByCategory(category);
        for(int i = 0;i < bookList.size();i++){
            String sTmp = bookList.get(i).getContentdescription();
            bookList.get(i).setContentdescription(sTmp.substring(0,sTmp.length()/2));
        }
        return bookList;
    }

    @Override
    public String selectBookByid(int id) {
        return bookMapper.selectBookById(id);
    }

    @Override
    public String selectBookPriceByid(int id) {
        return bookMapper.selectBookPriceById(id);
    }

    @Override
    public String selectBookAuthorByid(int id) {
        return bookMapper.selectBookAuthorById(id);
    }

    @Override
    public String selectBookContentById(int id) {
        return bookMapper.selectBookContentById(id);
    }

    @Override
    public Book selectBookByids(int bookid) {
        return bookMapper.selectBookByids(bookid);
    }

    @Override
    public Integer selectBookByNameAndAuthor(String bookname, String author) {
        return bookMapper.selectIdByBnAndAu(bookname,author);
    }

}
