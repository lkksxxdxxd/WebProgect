package com.cn.service.Impl;

import com.cn.dao.Storage;
import com.cn.mapper.StorageMapper;
import com.cn.service.storageServer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class storageServerImpl implements storageServer {
    @Autowired
    StorageMapper storageMapper;
    @Override
    public List<Storage> selectAllStorage() {
        return storageMapper.selectAllStorage();
    }

    @Override
    public List<Storage> selectStBy(String bookinfo, String category) {
        return storageMapper.selectStBy(bookinfo,category);
    }

    @Override
    public List<Storage> selectStByName(String bookinfo) {
        return storageMapper.selectByName(bookinfo);
    }
}
