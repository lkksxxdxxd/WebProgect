package com.cn.service.Impl;

import com.cn.dao.Book;
import com.cn.dao.Cart;
import com.cn.mapper.CartMapper;
import com.cn.service.CartServer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CartServerImpl implements CartServer {
    @Autowired
    CartMapper cartMapper;
    @Override
    public void insertBook(Book book,String username) {
        String name = book.getName();
        String author = book.getAuthor();
        int bookid = book.getId();
        int unitprice = book.getUnitprice();
        int num = 1;
        cartMapper.insertBook(bookid,name,author,unitprice,num,username);
    }

    @Override
    public Cart selectByUnAndBid(String username, int bookid) {
        return cartMapper.selectByUnAndBid(username,bookid);
    }

    @Override
    public void updateByUnAndBid(String username, int bookid) {
        cartMapper.updateByUnAndBid(username,bookid);
    }
    @Override
    public List<Cart> selectCartInfo(String username) {
        return cartMapper.selectCartInfo(username);
    }

    @Override
    public void deletByUnAndBn(String username, String bookname) {
        cartMapper.deletByUnAndBn(username,bookname);
    }

    @Override
    public void deletByUserName(String username) {
        cartMapper.deleteByUserName(username);
    }
}

