package com.cn.service.Impl;

import com.cn.dao.User;
import com.cn.mapper.UserMapper;
import com.cn.service.userLoginServer;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;

public class userLoginServerImpl implements userLoginServer {
    @Autowired
    User user;
    @Autowired
    UserMapper userMapper;
    @Override
    public User selectUserByUnAndPs(String username, String password) {
        return userMapper.selectByUnAndPs(username,password);
    }

    @Override
    public User selectUserByName(String username) {
        return userMapper.selectByName(username);
    }

    @Override
    public int insert(String username, String password) {
        String Salt = new SecureRandomNumberGenerator().nextBytes().toHex();
        //加密
        SimpleHash simpleHash = new SimpleHash("md5",password,Salt,1);
        String NewPassword = simpleHash.toString();
        return userMapper.insertUser(username,NewPassword,Salt);
    }
}
