package com.cn.service;

import com.cn.dao.InOrder;
import com.cn.dao.OutOrder;

import java.util.List;

public interface InorderServer {
    public List<InOrder> selectAllInorder();

    List<OutOrder> selectAllOutorder();
    void settlement(String bookname, String username,int quantity, int unitprice, String orderdate, String orderstate);
}
