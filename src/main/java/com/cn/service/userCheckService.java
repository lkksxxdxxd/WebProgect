package com.cn.service;
import com.cn.dao.Touxiang;
import com.cn.dao.Userall;
import org.apache.tomcat.jni.User;

public interface userCheckService {

    boolean insert(String username,String password);

    String[] getUsername();

    public boolean UserExitCheck(String[] Users,String tempUser);

    public Userall selectByKeyName(String user);
    Userall selectByNameAndPassword(String username, String password);
    void getId();

    String getUserPer(String user);

    void insertUserPer(String username, String id);

    Touxiang insertTouXiang(Integer id, String name);

    String getTouXiang(Integer id);
}
