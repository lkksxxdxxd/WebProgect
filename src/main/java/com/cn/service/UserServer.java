package com.cn.service;

import com.cn.dao.User;

import java.util.List;

public interface UserServer {
    List<User> selectUserInfo();
}
